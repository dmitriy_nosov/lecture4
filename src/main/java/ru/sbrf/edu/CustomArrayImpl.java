package ru.sbrf.edu;

import java.util.Arrays;
import java.util.Collection;

public class CustomArrayImpl<T> implements CustomArray<T> {

    private static final int DEFAULT_CAPACITY = 8;
    private Object[] elements;
    int size = 0;

    public CustomArrayImpl() {
        this(DEFAULT_CAPACITY);
    }

    public CustomArrayImpl(int capacity) {
        elements = new Object[capacity];
    }

    public CustomArrayImpl(Collection<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("items cannot be null");
        }
        if (items.isEmpty()) {
            throw new IllegalArgumentException("items cannot be empty");
        }
        size = items.size();
        this.elements = items.toArray();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T item) {
        if (size == elements.length) {
            elements = Arrays.copyOf(elements, elements.length + DEFAULT_CAPACITY);
        }
        elements[size] = item;
        size++;
        return true;
    }

    @Override
    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("items cannot be null");
        } else
            return addAll(size, items);
    }

    @Override
    public boolean addAll(Collection<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("items cannot be null!");
        }
        return addAll((T[]) (items.toArray()));
    }

    @Override
    public boolean addAll(int index, T[] items) {

        if (items == null) {
            throw new IllegalArgumentException("items cannot be null!");
        }
        if (items.length == 0) {
            return true;
        }
        if (index > elements.length) {
            throw new ArrayIndexOutOfBoundsException("index > elements.length");
        }
        if (index == 0) {
            Object[] tmp = new Object[elements.length + items.length];
            System.arraycopy(items, 0, tmp, 0, items.length);
            System.arraycopy(elements, 0, tmp, items.length, elements.length);
            size += items.length;
            elements = tmp;
        } else if (index == elements.length) {
            Object[] tmp = new Object[elements.length + items.length];
            System.arraycopy(elements, 0, tmp, 0, elements.length);
            System.arraycopy(items, 0, tmp, elements.length, items.length);
            size = tmp.length;
            elements = tmp;
        } else {
            Object[] tmp = new Object[elements.length + items.length];
            Object[] left = new Object[index];
            Object[] right = new Object[elements.length - index];

            System.arraycopy(elements, 0, left, 0, index - 1);
            System.arraycopy(elements, index, right, 0, elements.length - index);

            System.arraycopy(left, 0, tmp, 0, left.length);
            System.arraycopy(items, 0, tmp, left.length, items.length);
            System.arraycopy(right, 0, tmp, left.length + items.length, right.length);
            if (index <= size) {
                size += index;
            } else {
                size = index + (index - size);
            }
            elements = tmp;
        }
        return true;
    }

    @Override
    public T get(int index) {
        return (T) (elements[index]);
    }

    @Override
    public T set(int index, T item) {
        if (index < size) {
            elements[index] = item;
        } else if (index > elements.length) {
            throw new ArrayIndexOutOfBoundsException("index > elements.length");
        } else {
            elements[index] = item;
            size = index + 1;
        }
        return item;
    }

    @Override
    public void remove(int index) {
        Object[] newList = new Object[elements.length - 1];
        if (index <= size) {
            System.arraycopy(elements, 0, newList, 0, index);
            System.arraycopy(elements, index + 1, newList, index, elements.length - index - 1);
            elements = newList;
        } else {
            throw new ArrayIndexOutOfBoundsException("index > elements.length");
        }
    }

    @Override
    public boolean remove(T item) {
        for (int i = 0; i < elements.length; i++) {
            Object obj = elements[i];
            if (obj == null && item == null) {
                remove(i);
                return true;
            } else if (obj != null && obj.equals(item)) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(T item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null!");
        }
        for (Object value : elements) {
            if (value.equals(item)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public int indexOf(T item) {
        for (int i = 0; (i < elements.length); i++) {
            if (elements[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void ensureCapacity(int newElementsCount) {
        if (elements.length <= size + newElementsCount) {
            elements = Arrays.copyOf(elements, elements.length + newElementsCount);
            size = elements.length;
        }
    }

    @Override
    public int getCapacity() {
        return (elements.length - size);
    }

    @Override
    public void reverse() {
        Object[] reversed = new Object[elements.length];
        for (int i = 0; i < elements.length; i++) {
            reversed[i] = elements[elements.length - 1 - i];
        }
        elements = reversed;
    }

    @Override
    public String toString() {
        System.out.print("[ ");
        for (Object element : elements) {
            if (element != null) {
                System.out.print(element + " ");
            }
        }
        System.out.print("]\n");
        return Arrays.toString(elements);
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elements, elements.length);
    }
}