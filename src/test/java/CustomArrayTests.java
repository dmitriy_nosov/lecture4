import org.junit.Assert;
import org.junit.Test;
import ru.sbrf.edu.CustomArray;
import ru.sbrf.edu.CustomArrayImpl;

import java.util.*;

public class CustomArrayTests {

    /**
     * Test array size
     */
    @Test
    public void sizeTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(5);
        customArray.add(3);

        Assert.assertEquals(2, customArray.size());
    }

    /**
     * Test empty array
     */
    @Test
    public void isEmptyTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();

        Assert.assertEquals(customArray.size(), 0);
    }

    /**
     * Adding item to array
     */
    @Test
    public void addItemTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();

        customArray.add(7);
        customArray.add(3);

        Assert.assertEquals(Integer.valueOf(7), customArray.get(0));
    }

    /**
     * Adding items to array from massive
     */
    @Test
    public void addAllItemsFromMassiveTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        Integer[] array = new Integer[]{1, 2, 3};
        boolean result = customArray.addAll(array);

        Assert.assertTrue(result);
    }

    /**
     * Adding null massive to array
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllItemsFromMassiveExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        Integer[] array = null;
        customArray.addAll(array);
    }

    /**
     * Adding items to array from another collection
     */
    @Test
    public void addAllItemsFromCollectionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        Collection<Integer> array = new ArrayList<>();
        array.add(1);
        array.add(2);
        array.add(3);
        boolean result = customArray.addAll(array);

        Assert.assertEquals(3, customArray.size());
        Assert.assertTrue(result);
    }

    /**
     * Adding another null collection to array
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllItemsFromCollectionExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        Collection<Integer> array = null;
        customArray.addAll(array);
    }

    /**
     * Adding items from massive to array index
     */
    @Test
    public void addAllItemFromMassiveIntoIndexTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(5);
        Integer[] array = {1, 3, 5};
        customArray.addAll(2, array);

        Assert.assertEquals(1, (int) customArray.get(2));
        Assert.assertEquals(3, (int) customArray.get(3));
        Assert.assertEquals(5, (int) customArray.get(4));
    }

    /**
     * Adding items from massive to non-existing index of array
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addAllItemFromMassiveIntoIndexAIOOBExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(5);
        Integer[] array = new Integer[1];
        customArray.addAll(6, array);
    }

    /**
     * Adding null massive to array index
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllItemFromMassiveIntoIndexIAExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(5);
        Integer[] array = null;
        customArray.addAll(3, array);
    }

    /**
     * Getting item by index from array
     */
    @Test
    public void getItemByIndexTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(2);
        customArray.add(1);
        customArray.add(2);
        Assert.assertEquals(2, (int) customArray.get(1));
    }

    /**
     * Getting item by non-existing index from array
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getItemByIndexAIOOBExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(2);
        customArray.add(1);
        customArray.add(2);

        customArray.get(2);
    }

    /**
     * Setting item to array by index
     */
    @Test
    public void setIndexItemTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.set(1, 2);
        Assert.assertEquals(2, (int) customArray.get(1));
    }

    /**
     * Setting item to array by non-existing index
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setIndexItemAIOOBExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(2);
        customArray.set(1, 2);
        Assert.assertEquals(2, (int) customArray.get(3));
    }

    /**
     * Removing item from array by index
     */
    @Test
    public void removeIndexTest() {
        CustomArray<Object> customArray = new CustomArrayImpl<>(5);
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);

        customArray.remove(1);
        Assert.assertEquals(5, customArray.get(1));
    }

    /**
     * Removing item from array
     */
    @Test
    public void removeItemTest() {
        CustomArray<Object> customArray = new CustomArrayImpl<>(5);
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");

        customArray.remove("a");
        Assert.assertEquals("b", customArray.get(0));
    }

    /**
     * Check existing item
     */
    @Test
    public void containsItemTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);
        Assert.assertTrue(customArray.contains(3));
    }

    /**
     * Getting index of item from array
     */
    @Test
    public void indexOfItemTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);

        Assert.assertEquals(customArray.indexOf(5), 2);
    }

    /**
     * Getting index of non-existing item from array
     */
    @Test(expected = NullPointerException.class)
    public void indexOfItemNPExceptionTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.indexOf(2);
    }

    /**
     * Get capacity adequacy
     */
    @Test
    public void ensureCapacityTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(3);
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);

        customArray.ensureCapacity(2);

        Assert.assertEquals(5, customArray.size());
    }

    /**
     * Get array capacity
     */
    @Test
    public void getCapacityTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>(5);
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);

        Assert.assertEquals(2, customArray.getCapacity());
    }

    /**
     * Get reverse array
     */
    @Test
    public void reverseTest() {
        CustomArray<Object> customArray = new CustomArrayImpl<>(3);
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);

        customArray.reverse();

        Assert.assertEquals(5, (int) customArray.get(0));
        Assert.assertEquals(3, (int) customArray.get(1));
        Assert.assertEquals(1, (int) customArray.get(2));
    }

    /**
     * Get copy of array
     */
    @Test
    public void toArrayTest() {
        CustomArray<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(3);
        customArray.add(5);
        Object[] newArray = customArray.toArray();
        Assert.assertEquals(1, (int) newArray[0]);
        Assert.assertEquals(3, (int) newArray[1]);
        Assert.assertEquals(5, (int) newArray[2]);
    }
}